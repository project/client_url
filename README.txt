Client URL
================================================================================

Occasionally, you need to show a client several different color options for a 
page or highlight a particular element in a layout for a customer, but you don't 
want any other users to see those changes. This module allows you to do all of 
that and more, by modifying css based on a URL.

Here's how it works: by adding a special query string to your URL, you can 
assign a css class to a particular target element(s) within the page. You then 
add an additional rule to your theme's css file for the new class, which adds 
(or overrides) css styles for the target element; this rule could add a 
highlight color, show an alternate background color, or unhide a form element. 
You can send your client the modified URL and only they will be able to see the 
new css you've added.


Examples
================================================================================
Add a class called "client" to a target element called "field-of-interest" on 
the home page:
http://mysite.com?client_target=.field-of-interest

Add a class called "client" to a target element called "field-of-interest" on 
the home page AND scroll to that section of the page: 
http://mysite.com?client_target=.field-of-interest&client_scrollto=true

Do all of the above, but use a shortcut instead, so that the URL is shorter:
http://mysite.com?client=interest


A Note About CSS Selectors
================================================================================
It's ok to use compound css selectors in the query string, but you'll need to
replace spaces with the "+" character and since the "#" character is reserved
for URL anchors, you'll need to use a "~" (tilde) instead in css id names. For 
example, if you had the following css selector (for a target element):
#content-area .field-of-interest

Then, the query string would look like this:
http://mysite.com?client_target=~content-area+.field-of-interest


Configuration
================================================================================
The Client URL module can be configured by going to:
Admin >> Site Configuration >> Client URL

The configuration settings are as follows:
* Base Class Name - The default base class name is "client", but you can change
it to anything you'd like. Two classes will be added to the target element, 
using this base name: client and client-X (where X depends on the number of 
matching elements).

* Wrap target element in a new <div>, with classes applied - By default, the 
module just adds additional classes to the target element, and does not create a 
new <div>. This option allows you to override that behavior in cases where
you need a new <div> element wrapped around the target element.

* Scroll to the target element - Enabling this option tells the module to use the 
ScrollTo jquery plugin to scroll to the target element. This is useful when you 
need to direct a client (or customer) to a particular section within a long page.
Scrolling is limited to cases where there is only one matching element.

* Add Client URL shortcuts - You can enter Client URL shortcuts by entering one
key|value item per line, where "key" is the shortcut name and "value" is the 
query string.

Example: eml|client_target=.field-type-email

This means that going to mysite.com?client=eml (the shortcut) would be the same 
as entering mysite.com?client_target=.field-type-email as the URL.

The best way to create shortcuts is to test the query string out in a URL first, 
then once you've verified that it's working, copy the URL's query string 
(everything after the "?") into the value of the key|value pair, and add a 
shortcut name (as the key). You can include any settings in the shortcut's query 
string that would appear in the URL query string; see the next section for more 
details.


Overriding settings from the URL
================================================================================
Any configuration settings for this module (see section above) can be overrided 
by adding variables to the query string (in the URL or in a Client URL 
shortcut). This is useful in cases where you have several Client URLs in use,
but only one of them needs to use different settings than those specified on the 
configuration page (such as automatic scrolling). Here are examples for 
overriding each setting from the URL:

Automatic scrolling behavior:
Disable: http://mysite.com?client_target=.field-of-interest&client_scrollto=0
Enable: http://mysite.com?client_target=.field-of-interest&client_scrollto=true

Wrap target in <div>:
Disable: http://mysite.com?client_target=.field-of-interest&client_wrap=0
Enable: http://mysite.com?client_target=.field-of-interest&client_wrap=true

Set default base class:
http://mysite.com?client_target=.field-of-interest&client_class=highlight
