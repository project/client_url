<?php

function client_url_settings() {
	$form['client_url_base_class'] = array(
		'#type' => 'textfield',
		'#title' => t('Base class name'),
		'#description' => t('Two classes will be added to the target element, using this base name: client and client-X (where X depends on the number of matching elements).'),
		'#default_value' => variable_get('client_url_base_class', 'client'),
  	'#size' => 20,
  	'#maxlength' => 20,
	);
	$form['client_url_wrap_mode'] = array(
		'#type' => 'checkboxes',
		'#title' => t(''),
		'#options' => array('wrap' => t('Wrap target element in a new &lt;div&gt;, with classes applied.')),
		'#description' => t('By default, the module just adds additional classes to the target element, and does not create a new &lt;div&gt;.'),
		'#default_value' => variable_get('client_url_wrap_mode', array('wrap' => 0)),
	);
	$form['client_url_scrollto'] = array(
		'#type' => 'checkboxes',
		'#title' => t(''),
		'#options' => array('scrollto' => t('Scroll to the target element.')),
		'#description' => t('This uses the ScrollTo jquery plugin to scroll to the target element (limited to cases where there is only one matching element).'),
		'#default_value' => variable_get('client_url_scrollto', array('scrollto' => 0)),
	);
	$form['client_url_shortcuts'] = array(
		'#type' => 'textarea',
		'#title' => t('Add Client URL shortcuts'),
		'#description' => t('Enter one key|value per line, where "key" is the shortcut name and "value" is the query string. Example: eml|client_target=.field-type-email (this means that going to mysite.com?client=eml would be the same as entering mysite.com?client_target=.field-type-email).'),
		'#default_value' => variable_get('client_url_shortcuts', ''),
	);				
	return system_settings_form($form);
}