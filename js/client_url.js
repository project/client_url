Drupal.behaviors.client_url = function() {
	// get url, including query string
  var get_query = document.location.search;
		
	// only proceed if a client_url var is detected	
  if (get_query.match(/client_target/) || get_query.match(/client=/)) {
  	// get var from query string, translating '+' characters to spaces
  	// and '~' characters to '#' (we're using tilde instead of hash mark for css id selector character)
  	var client_target = getVar('client_target').replace(/~/g, "#");
		var class_type = Drupal.settings.client_url.client_url_wrap_mode; // default is 'add'
		var class_name = Drupal.settings.client_url.client_url_base_class;  // default is 'client'
		var shortcuts_list = Drupal.settings.client_url.client_url_shortcuts; // list of shortcuts
		var scrollto = Drupal.settings.client_url.client_url_scrollto; // default is scrollto off
		var num_matches = 0;
		
		// get options and overrides from query string
		if (get_query.match(/client_class/)) {
			class_name = getVar('client_class');
		}	
		if (get_query.match(/client_wrap/)) {
			class_type['wrap'] = getVar('client_wrap');
		}
		if (get_query.match(/client_scrollto/)) {
			scrollto['scrollto'] = getVar('client_scrollto');
		}
		
		// if client var is being used, need to search for matching shortcuts and possibly
		// override other options already set above
		if (get_query.match(/client=/)) {
			// test for each possible shortcut, assigning corresponding query string for each one found
			for (var key in shortcuts_list) {
    		var shortcut_value = shortcuts_list[key];
			
    		if (getVar('client') == key) {
    			// set all options that are listed in the shortcut value
    			client_target = (getVar('client_target', shortcut_value) != '') ? getVar('client_target', shortcut_value).replace(/~/g, "#") : client_target;
    			class_name = (getVar('client_class', shortcut_value) != '') ? getVar('client_class', shortcut_value) : class_name;
    			class_type['wrap'] = (getVar('client_wrap', shortcut_value) != '') ? getVar('client_wrap', shortcut_value) : class_type['wrap'];
    			scrollto['scrollto'] = (getVar('client_scrollto', shortcut_value) != '') ? getVar('client_scrollto', shortcut_value) : scrollto['scrollto'];
    			num_matches = add_classes(class_type, client_target, class_name);
    		}	
			}
		}
		else {
			num_matches = add_classes(class_type, client_target, class_name);
		}
		if (scrollto['scrollto'] != 0 && num_matches == 1) {
			$.scrollTo(client_target, 800);
		}
  }
}


// either add classes to the target element (default), or wrap it in a new
// div with classes applied to it.
function add_classes(class_type, client_target, class_name) {
	var i = 0;
	if (class_type['wrap'] != 0) {
		$(client_target).each(function(index) {
			i = index + 1;
			$(this).wrap('<div class="' + class_name + ' ' + class_name + '-' + i + '" />');
		});
	}
	else {
		$(client_target).each(function(index) {
			i = index + 1;
			$(this).addClass(class_name);
			$(this).addClass(class_name + '-' + i);
		});
	}
	return i;	
}


//
// function to access GET variables
// by default, use query string, but can use other strings too
// based on code from http://scripts.franciscocharrua.com/javascript-get-variables.php
//

function getVar(name, get_string) {
      
   // optional parameter with default value
   get_string = (typeof get_string == 'undefined') ? document.location.search : get_string;        
   return_value = '';
 
   do { //This loop is made to catch all instances of any get variable.
      name_index = get_string.indexOf(name + '=');
      
      if(name_index != -1)
        {
        get_string = get_string.substr(name_index + name.length + 1, get_string.length - name_index);
        
        end_of_value = get_string.indexOf('&');
        if(end_of_value != -1)                
          value = get_string.substr(0, end_of_value);                
        else                
          value = get_string;                
          
        if(return_value == '' || value == '')
           return_value += value;
        else
           return_value += ', ' + value;
        }
   } while(name_index != -1)
      
   //Restores all the blank spaces.
   space = return_value.indexOf('+');
   while(space != -1)
        { 
        return_value = return_value.substr(0, space) + ' ' + 
        return_value.substr(space + 1, return_value.length);
				 
        space = return_value.indexOf('+');
        }
    
   return(return_value);        
}